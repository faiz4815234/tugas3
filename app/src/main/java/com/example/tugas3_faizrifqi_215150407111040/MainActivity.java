package com.example.tugas3_faizrifqi_215150407111040;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int[] checkBoxIds = {R.id.checkBoxRambut, R.id.checkBoxAlis, R.id.checkBoxKumis, R.id.checkBoxJanggut};
        final int[] imageViewIds = {R.id.imageViewRambut, R.id.imageViewAlis, R.id.imageViewKumis, R.id.imageViewJanggut};
        for (int i = 0; i < checkBoxIds.length; i++) {
            final CheckBox checkBox = findViewById(checkBoxIds[i]);
            checkBox.setChecked(true);
            final ImageView imageView = findViewById(imageViewIds[i]);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }
}